/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pongsakon.exhaustivesearch;

import java.util.Scanner;

/**
 *
 * @author 66955
 */
public class ExhaustiveSearch {

    public int maxProfit(int[] prices) {
        int minPriceSoFar = Integer.MAX_VALUE, maxProfitSoFar = 0;

        for (int i = 0; i < prices.length; i++) {
            if (minPriceSoFar > prices[i]) {
                minPriceSoFar = prices[i];
            } else {
                maxProfitSoFar = Math.max(maxProfitSoFar, prices[i] - minPriceSoFar);
            }
        }

        return maxProfitSoFar;
    }

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        int[] prices = {10, 40, 25, 10, 6,};
        int[] prices2 = {7, 1, 5, 3, 6, 4};
        ExhaustiveSearch ex = new ExhaustiveSearch();
        for (int i = 0; i < prices.length; i++) {
            System.out.print(prices[i] + " ");
        }
        System.out.println("");
        System.out.print("MaxProfit = ");
        System.out.println(ex.maxProfit(prices));

    }
}
